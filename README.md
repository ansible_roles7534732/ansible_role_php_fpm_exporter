php_fpm_exporter role
=========

Installs php-fpm_exporter(https://github.com/hipages/php-fpm_exporter)

Requirements
------------

—

Role Variables
--------------

See defaults/main.yml

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install php-fpm_exporter
  hosts: db_vms
  become: true
  gather_facts: true
  roles:
    - php_fpm_exporter
      tags:
        - role_php_fpm_exporter
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
